package com.example;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ScientificCalculatorTest {

    private ScientificCalculator calculator;

    @BeforeEach
    public void setup() {
        calculator = new ScientificCalculator();
    }

    @Test
    public void addition() {
        assertThat(calculator.add(1.0, 2.0), is(3.0));
    }

    @Test
    public void testSubtract() {
        assertThat(calculator.subtract(5.0, 3.0), is(2.0));
    }

    @Test
    public void testMultiply() {
        assertThat(calculator.multiply(2.0, 3.0), is(6.0));
    }

    @Test
    public void testDivide() {
        assertThat(calculator.divide(6.0, 3.0), is(2.0));
    }

    @Test
    public void testDivideByZero() {
        try {
            calculator.divide(5.0, 0.0);
            Assert.fail("Expected an ArithmeticException");
        } catch (ArithmeticException e) {
            // Test passed
        }
    }

    @Test
    public void testMin() {
        assertThat(calculator.min(2.0, 3.0), is(2.0));
    }

    @Test
    public void testMax() {
        assertThat(calculator.max(2.0, 3.0), is(3.0));
    }

    @Test
    public void testSquareRoot() {
        assertThat(calculator.squareroot(4.0), is(2.0));
    }

    @Test
    public void testCalculateAddition() {
        assertThat(calculator.calculate("+", 2.0, 3.0), is(5.0));
    }

    @Test
    public void testCalculateSubtraction() {
        assertThat(calculator.calculate("-", 5.0, 3.0), is(2.0));
    }

    @Test
    public void testCalculateMultiplication() {
        assertThat(calculator.calculate("*", 2.0, 3.0), is(6.0));
    }

    @Test
    public void testCalculateDivision() {
        assertThat(calculator.calculate("/", 6.0, 3.0), is(2.0));
    }

    @Test
    public void testCalculateDivisionByZero() {
        try {
            calculator.calculate("/", 5.0, 0.0);
            Assert.fail("Expected an ArithmeticException");
        } catch (ArithmeticException e) {
            // Test passed
        }
    }

    @Test
    public void testCalculateMin() {
        assertThat(calculator.calculate("min", 2.0, 3.0), is(2.0));
    }

    @Test
    public void testCalculateMax() {
        assertThat(calculator.calculate("max", 2.0, 3.0), is(3.0));
    }

    @Test
    public void testCalculateSquareRoot() {
        assertThat(calculator.calculate("sqrt", 4.0, 0), is(2.0));
    }

    @Test
    public void testCalculateUnknownOperation() {
        assertThat(calculator.calculate("unknown", 2.0, 3.0), is(0.0));
    }

}
