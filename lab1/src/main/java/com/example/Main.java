package com.example;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Calculator calculator = new ScientificCalculator();
        boolean continueCalculating = true;

        while (continueCalculating){
            System.out.print("Enter operation (+, -, *, /, min, max, sqrt) or 'exit' to quit: ");

            String operator = scanner.next();

            if (operator.equalsIgnoreCase("exit")) {
                break;
            }

            if (!operator.matches("[+\\-*/]|min|max|sqrt")) {
                System.out.println("Invalid operator. Please try again.");
                continue;
            }

            System.out.print("Enter the first operand: ");
            double operand1 = scanner.nextDouble();

            if (!operator.equalsIgnoreCase("sqrt")) {
                System.out.print("Enter the second operand: ");
                double operand2 = scanner.nextDouble();

                try {
                    double result = calculator.calculate(operator, operand1, operand2);
                    System.out.println("Result: " + result);
                } catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                }
            } else {
                try {
                    double result = calculator.calculate(operator, operand1, 0);
                    System.out.println("Result: " + result);
                } catch (Exception e) {
                    System.out.println("Error: " + e.getMessage());
                }
            }
        }

        scanner.close();
    }

}
