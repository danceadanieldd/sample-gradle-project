package com.example;

public class ScientificCalculator implements Calculator {
    @Override
    public double add(double x, double y) {
        return x + y;
    }

    @Override
    public double subtract(double x, double y){
        return x - y;
    }

    @Override
    public double multiply(double x, double y){
        return x * y;
    }

    @Override
    public double divide(double x, double y){
        if (y == 0) throw new ArithmeticException("Division by zero is not allowed");
        return x/y;
    }

    public double min(double x, double y){
        return Math.min(x,y);
    }

    public double max(double x, double y){
        return Math.max(x,y);
    }

    public double squareroot(double x){
        return Math.sqrt(x);
    }

    @Override
    public double calculate(String operation, double x, double y) {
        switch (operation){
            case "+":
                return this.add(x, y);
            case "-":
                return this.subtract(x, y);
            case "*":
                return this.multiply(x, y);
            case "/":
                if (y != 0) {
                    return this.divide(x,y);
                } else {
                    throw new ArithmeticException("Division by zero is not allowed");
                }
            case "min":
                return this.min(x, y);
            case "max":
                return this.max(x, y);
            case "sqrt":
                return this.squareroot(x);
        }
        return 0;
    }

}
