package com.example;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapRepository<T> implements InMemoryRepository<T>{
    private ConcurrentHashMap<T, Boolean> map;

    public ConcurrentHashMapRepository() {
        this.map = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T item) {
        map.put(item, true);
    }

    @Override
    public boolean contains(T item) {
        return map.containsKey(item);
    }

    @Override
    public void remove(T item) {
        map.remove(item);
    }
}
