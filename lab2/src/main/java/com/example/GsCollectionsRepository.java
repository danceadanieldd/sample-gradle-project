package com.example;
import org.eclipse.collections.api.collection.MutableCollection;
import org.eclipse.collections.impl.factory.Lists;

import java.util.List;

public class GsCollectionsRepository<T> implements InMemoryRepository<T>{
    private MutableCollection<T> collection;

    public GsCollectionsRepository() {
        this.collection = Lists.mutable.empty();
    }

    @Override
    public void add(T item) {
        collection.add(item);
    }

    @Override
    public boolean contains(T item) {
        return collection.contains(item);
    }

    @Override
    public void remove(T item) {
        collection.remove(item);
    }
}
