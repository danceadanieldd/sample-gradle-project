package com.example;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

@Fork(1)  // Number of forked JVMs for each benchmark
@Warmup(iterations = 3)  // Number of warm-up iterations
@Measurement(iterations = 5)  // Number of measurement iterations

@State(Scope.Benchmark)
public class RepositoryBenchmark {

    static int n = 1000;

    private ArrayListBasedRepository<Order> arrayListBasedRepository;

    private ConcurrentHashMapRepository<Order> concurrentHashMapBasedRepository;

    private FastutilRepository<Order> fastutilRepository;

    private HashSetBasedRepository<Order> hashSetBasedRepository;

    private TreeSetBasedRepository<Order> treeSetBasedRepository;

    private GsCollectionsRepository<Order> gsCollectionsRepository;


    @Setup
    public void setup(){
        arrayListBasedRepository = new ArrayListBasedRepository<>();
        concurrentHashMapBasedRepository = new ConcurrentHashMapRepository<>();
        fastutilRepository = new FastutilRepository<>();
        hashSetBasedRepository = new HashSetBasedRepository<>();
        treeSetBasedRepository = new TreeSetBasedRepository<>();
        gsCollectionsRepository = new GsCollectionsRepository<>();
    }

    @Benchmark
    public void benchmarkArrayListBasedRepositoryAdd(){
        for (int i = 1; i <= n; i++)
        {
            arrayListBasedRepository.add(new Order(i,i,i));
        }
    }
    @Benchmark
    public void benchmarkArrayListBasedRepositoryContains(){
        for (int i = 1; i <= n; i++)
        {
            arrayListBasedRepository.contains(new Order(i,i,i));
        }
    }
    @Benchmark
    public void benchmarkArrayListBasedRepositoryRemove(){
        for (int i = 1; i <= n; i++)
        {
            arrayListBasedRepository.remove(new Order(i,i,i));
        }
    }








    @Benchmark
    public void benchmarkConcurrentHashMapBasedRepositoryAdd() {
        for (int i = 1; i <= n; i++) {
            concurrentHashMapBasedRepository.add(new Order(i, i, i));
        }
    }
    @Benchmark
    public void benchmarkConcurrentHashMapBasedRepositoryContains() {
        for (int i = 1; i <= n; i++) {
            concurrentHashMapBasedRepository.contains(new Order(i, i, i));
        }
    }
    @Benchmark
    public void benchmarkConcurrentHashMapBasedRepositoryRemove() {
        for (int i = 1; i <= n; i++) {
            concurrentHashMapBasedRepository.remove(new Order(i, i, i));
        }
    }

    @Benchmark
    public void benchmarkFastutilRepositoryAdd() {
        for (int i = 1; i <= n; i++) {
            fastutilRepository.add(new Order(i, i, i));
        }
    }
    @Benchmark
    public void benchmarkFastutilRepositoryContains() {
        for (int i = 1; i <= n; i++) {
            fastutilRepository.contains(new Order(i, i, i));
        }
    }
    @Benchmark
    public void benchmarkFastutilRepositoryRemove() {
        for (int i = 1; i <= n; i++) {
            fastutilRepository.remove(new Order(i, i, i));
        }
    }

    @Benchmark
    public void benchmarkHashSetBasedRepositoryAdd() {
        for (int i = 1; i <= n; i++) {
            hashSetBasedRepository.add(new Order(i, i, i));
        }
    }
    @Benchmark
    public void benchmarkHashSetBasedRepositoryContains() {
        for (int i = 1; i <= n; i++) {
            hashSetBasedRepository.contains(new Order(i, i, i));
        }
    }
    @Benchmark
    public void benchmarkHashSetBasedRepositoryRemove() {
        for (int i = 1; i <= n; i++) {
            hashSetBasedRepository.remove(new Order(i, i, i));
        }
    }

    @Benchmark
    public void benchmarkTreeSetBasedRepositoryAdd() {

        for (int i = 1; i <= n; i++) {
            treeSetBasedRepository.add(new Order(i,i,i));
        }
    }
    @Benchmark
    public void benchmarkTreeSetBasedRepositoryContains() {
        for (int i = 1; i <= n; i++) {
            treeSetBasedRepository.contains(new Order(i,i,i));
        }
    }
    @Benchmark
    public void benchmarkTreeSetBasedRepositoryRemove() {
        for (int i = 1; i <= n; i++) {
            treeSetBasedRepository.remove(new Order(i,i,i));
        }
    }

    @Benchmark
    public void benchmarkGcCollectionRepositoryAdd() {
        for (int i = 1; i <= n/2; i++) {
            gsCollectionsRepository.add(new Order(i,i,i));
        }
    }
    @Benchmark
    public void benchmarkGcCollectionRepositoryContains() {
        for (int i = 1; i <= n; i++) {
            gsCollectionsRepository.contains(new Order(i,i,i));
        }
    }
    @Benchmark
    public void benchmarkGcCollectionRepositoryRemove() {
        for (int i = 1; i <= n; i++) {
            gsCollectionsRepository.remove(new Order(i,i,i));
        }
    }



}