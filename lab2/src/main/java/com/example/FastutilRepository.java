package com.example;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;

public class FastutilRepository<T> implements InMemoryRepository<T>{
    private ObjectOpenHashSet<T> set;

    public FastutilRepository() {
        this.set = new ObjectOpenHashSet<>();
    }

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }
}
